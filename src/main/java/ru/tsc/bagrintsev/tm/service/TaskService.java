package ru.tsc.bagrintsev.tm.service;

import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.api.sevice.ITaskService;
import ru.tsc.bagrintsev.tm.enumerated.Entity;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.field.*;
import ru.tsc.bagrintsev.tm.exception.user.AbstractUserException;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.Date;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(final String userId, final String name) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.NAME, name);
        return repository.create(userId, name);
    }

    @Override
    public Task create(final String userId, final String name, String description) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.NAME, name);
        check(EntityField.DESCRIPTION, description);
        return repository.create(userId, name, description);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) throws AbstractException {
        check(EntityField.PROJECT_ID, projectId);
        return repository.findAllByProjectId(userId, projectId);
    }

    @Override
    public Task updateByIndex(final String userId, final Integer index, final String name, final String description) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(index);
        check(EntityField.NAME, name);
        final Task task = findOneByIndex(userId, index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateById(final String userId, final String id, final String name, final String description) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.TASK_ID, id);
        check(EntityField.NAME, name);
        final Task task = findOneById(userId, id);
        check(Entity.TASK, task);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final String userId, final Integer index, final Status status) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(index);
        final Task task = findOneByIndex(userId, index);
        task.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) {
            task.setDateStarted(new Date());
        } else if (status.equals(Status.COMPLETED)) {
            task.setDateFinished(new Date());
        } else if (status.equals(Status.NOT_STARTED)) {
            task.setDateStarted(null);
            task.setDateFinished(null);
        }
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String userId, final String id, final Status status) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.TASK_ID, id);
        final Task task = findOneById(userId, id);
        check(Entity.TASK, task);
        task.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) {
            task.setDateStarted(new Date());
        } else if (status.equals(Status.COMPLETED)) {
            task.setDateFinished(new Date());
        } else if (status.equals(Status.NOT_STARTED)) {
            task.setDateStarted(null);
            task.setDateFinished(null);
        }
        return task;
    }

}
