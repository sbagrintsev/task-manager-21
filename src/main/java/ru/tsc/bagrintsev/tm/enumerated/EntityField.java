package ru.tsc.bagrintsev.tm.enumerated;

public enum EntityField {
    ID("id"),
    NAME("name"),
    DESCRIPTION("description"),
    STATUS("status"),
    PROJECT_ID("project id"),
    TASK_ID("task id"),
    LOGIN("login"),
    PASSWORD("password"),
    EMAIL("email"),
    FIRST_NAME("first name"),
    MIDDLE_NAME("middle name"),
    LAST_NAME("last name"),
    ROLE("role"),
    USER_ID("user id"),
    COMMAND_NAME("name"),
    COMMAND_SHORT("short name");

    private final String displayName;

    EntityField(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
