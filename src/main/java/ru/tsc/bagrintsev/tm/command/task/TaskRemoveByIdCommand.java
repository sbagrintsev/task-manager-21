package ru.tsc.bagrintsev.tm.command.task;

import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

public class TaskRemoveByIdCommand extends AbstractTaskCommand {
    @Override
    public void execute() throws IOException, AbstractException {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().removeById(userId, id);
    }

    @Override
    public String getName() {
        return "task-remove-by-id";
    }

    @Override
    public String getDescription() {
        return "Remove task by id.";
    }
}
