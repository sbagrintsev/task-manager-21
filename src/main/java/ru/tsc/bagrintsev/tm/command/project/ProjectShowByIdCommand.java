package ru.tsc.bagrintsev.tm.command.project;

import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

public class ProjectShowByIdCommand extends AbstractProjectCommand {
    @Override
    public void execute() throws IOException, AbstractException {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        final Project project = getProjectService().findOneById(userId, id);
        showProject(project);
    }

    @Override
    public String getName() {
        return "project-show-by-id";
    }

    @Override
    public String getDescription() {
        return "Show project by id.";
    }
}
