package ru.tsc.bagrintsev.tm.command.task;

import ru.tsc.bagrintsev.tm.api.sevice.IProjectTaskService;
import ru.tsc.bagrintsev.tm.api.sevice.ITaskService;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.util.DateUtil;

public abstract class AbstractTaskCommand extends AbstractCommand {


    public ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    public IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @Override
    public String getShortName() {
        return "";
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showTask(final Task task) {
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(task.getDateCreated()));
        System.out.println("STARTED: " + DateUtil.toString(task.getDateStarted()));
        System.out.println("FINISHED: " + DateUtil.toString(task.getDateFinished()));
    }

}
