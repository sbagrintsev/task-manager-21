package ru.tsc.bagrintsev.tm.command.task;

import ru.tsc.bagrintsev.tm.exception.AbstractException;

public class TaskClearCommand extends AbstractTaskCommand {
    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK CLEAR]");
        final String userId = getUserId();
        getTaskService().clear(userId);
    }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }
}
