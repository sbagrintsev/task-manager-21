package ru.tsc.bagrintsev.tm.command.project;

import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

public class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException, IOException {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        getProjectService().changeProjectStatusByIndex(userId, index, Status.COMPLETED);
    }

    @Override
    public String getName() {
        return "project-complete-by-index";
    }

    @Override
    public String getDescription() {
        return "Complete project by index.";
    }

}
