package ru.tsc.bagrintsev.tm.command.user;

import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class UserChangePasswordCommand extends AbstractUserCommand{

    @Override
    public void execute() throws GeneralSecurityException, IOException, AbstractException {
        showOperationInfo();
        showParameterInfo(EntityField.LOGIN);
        final String login = TerminalUtil.nextLine();
        final String userId = getUserService().findByLogin(login).getId();
        showParameterInfo(EntityField.PASSWORD);
        final String password = TerminalUtil.nextLine();
        try {
            getUserService().setPassword(userId, password);
        } catch (IdIsEmptyException e) {
            throw new UserNotFoundException();
        }
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public String getName() {
        return "user-change-password";
    }

    @Override
    public String getDescription() {
        return "Change user password.";
    }

}
