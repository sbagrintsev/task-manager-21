package ru.tsc.bagrintsev.tm.command.system;

public class VersionCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("task-manager version 1.21.0");
    }

    @Override
    public String getName() {
        return "version";
    }

    @Override
    public String getShortName() {
        return "-v";
    }

    @Override
    public String getDescription() {
        return "Print version.";
    }

}
