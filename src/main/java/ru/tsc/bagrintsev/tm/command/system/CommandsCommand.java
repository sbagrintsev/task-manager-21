package ru.tsc.bagrintsev.tm.command.system;

import ru.tsc.bagrintsev.tm.api.model.ICommand;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandsCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[Interaction commands]");
        Collection<AbstractCommand> repository = getCommandService().getAvailableCommands();
        repository.stream()
                .filter(c -> !c.getName().isEmpty() && c.getName() != null)
                .forEach(c -> System.out.printf("%-35s%s\n", c.getName(), c.getDescription()));
    }

    @Override
    public String getName() {
        return "commands";
    }

    @Override
    public String getShortName() {
        return "-cmd";
    }

    @Override
    public String getDescription() {
        return "Print application interaction commands.";
    }

}
