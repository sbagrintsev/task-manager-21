package ru.tsc.bagrintsev.tm.command.task;

import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.field.AbstractFieldException;
import ru.tsc.bagrintsev.tm.exception.user.AbstractUserException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

public class TaskCreateCommand extends AbstractTaskCommand {
    @Override
    public void execute() throws IOException, AbstractException {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().create(userId, name, description);
    }

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }
}
