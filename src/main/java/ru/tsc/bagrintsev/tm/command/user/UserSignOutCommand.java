package ru.tsc.bagrintsev.tm.command.user;

import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

import java.io.IOException;

public class UserSignOutCommand extends AbstractUserCommand{
    @Override
    public void execute() throws IOException, AbstractException {
        showOperationInfo();
        getAuthService().signOut();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public String getName() {
        return "user-sign-out";
    }

    @Override
    public String getDescription() {
        return "Sign user out.";
    }

}
