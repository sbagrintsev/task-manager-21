package ru.tsc.bagrintsev.tm.command.task;

import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;
import java.util.Arrays;

public class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {
    @Override
    public void execute() throws IOException, AbstractException {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS: ");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final String userId = getUserId();
        getTaskService().changeTaskStatusByIndex(userId, index, status);
    }

    @Override
    public String getName() {
        return "task-change-status-by-index";
    }

    @Override
    public String getDescription() {
        return "Change task status by index.";
    }
}
