package ru.tsc.bagrintsev.tm.command.system;

public class ExitCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getShortName() {
        return "";
    }

    @Override
    public String getDescription() {
        return "Close program.";
    }

}
