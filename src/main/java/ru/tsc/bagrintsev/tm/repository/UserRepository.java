package ru.tsc.bagrintsev.tm.repository;

import ru.tsc.bagrintsev.tm.api.repository.IUserRepository;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectParameterNameException;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.bagrintsev.tm.util.HashUtil;

import java.security.GeneralSecurityException;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User create(final String login, final String password) throws GeneralSecurityException {
        final User user = new User();
        user.setLogin(login);
        setUserPassword(user, password);
        return add(user);
    }
    @Override
    public User setParameter(final User user,
                             final EntityField paramName,
                             final String paramValue) throws IncorrectParameterNameException {
        switch (paramName) {
            case EMAIL:
                user.setEmail(paramValue);
                break;
            case FIRST_NAME:
                user.setFirstName(paramValue);
                break;
            case MIDDLE_NAME:
                user.setMiddleName(paramValue);
                break;
            case LAST_NAME:
                user.setLastName(paramValue);
                break;
            default: throw new IncorrectParameterNameException(paramName, "User");
        }
        return user;
    }

    @Override
    public User setRole(final User user, final Role role) {
        user.setRole(role);
        return user;
    }

    @Override
    public void setUserPassword(final User user, final String password) throws GeneralSecurityException {
        byte[] salt = HashUtil.generateSalt();
        user.setPasswordSalt(salt);
        user.setPasswordHash(HashUtil.generateHash(password, salt));
    }

    @Override
    public User findOneById(final String id) throws AbstractException {
        return records.stream()
                .filter(record -> id.equals(record.getId()))
                .findFirst()
                .orElseThrow(() -> new UserNotFoundException("Id", id));
    }

    @Override
    public User findByLogin(final String login) throws UserNotFoundException {
        return records.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst()
                .orElseThrow(() -> new UserNotFoundException("Login", login));
    }

    @Override
    public User findByEmail(final String email) throws UserNotFoundException {
        return records.stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst()
                .orElseThrow(() -> new UserNotFoundException("Email", email));
    }

    @Override
    public User removeByLogin(final String login) throws UserNotFoundException {
        final User user = findByLogin(login);
        return remove(user);
    }

}
