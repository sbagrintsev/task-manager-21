package ru.tsc.bagrintsev.tm.repository;

import ru.tsc.bagrintsev.tm.api.repository.IAbstractRepository;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IAbstractRepository<M> {

    protected final List<M> records = new ArrayList<>();

    @Override
    public M add(final M record) {
        records.add(record);
        return record;
    }

    @Override
    public List<M> findAll() {
        return records;
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        return records.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public M findOneByIndex(final Integer index) {
        return records.get(index);
    }

    @Override
    public M findOneById(final String id) throws AbstractException {
        return records.stream()
                .filter(record -> id.equals(record.getId()))
                .findFirst()
                .orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public boolean existsById(final String id) throws AbstractException {
        findOneById(id);
        return true;
    }

    @Override
    public M remove(final M record) {
        records.remove(record);
        return record;
    }

    @Override
    public M removeByIndex(final Integer index) throws ModelNotFoundException {
        final M record = findOneByIndex(index);
        if (record == null) throw new ModelNotFoundException();
        return remove(record);
    }

    @Override
    public M removeById(final String id) throws AbstractException {
        final M record = findOneById(id);
        return remove(record);
    }

    @Override
    public int totalCount() {
        return records.size();
    }

    @Override
    public void clear() {
        records.clear();
    }

    @Override
    public void removeAll(final Collection<M> collection) {
        records.removeAll(collection);
    }

}
