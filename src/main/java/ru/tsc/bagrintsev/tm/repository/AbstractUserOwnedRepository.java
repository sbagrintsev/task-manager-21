package ru.tsc.bagrintsev.tm.repository;

import ru.tsc.bagrintsev.tm.api.repository.IUserOwnedRepository;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.model.AbstractUserOwnedModel;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public M add(final String userId, final M record) {
        record.setUserId(userId);
        return add(record);
    }

    @Override
    public List<M> findAll(final String userId) {
        return records.stream()
                .filter(record -> userId.equals(record.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        return records.stream()
                .filter(record -> userId.equals(record.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public M findOneById(final String userId, final String id) throws AbstractException {
        return records.stream()
                .filter(record -> userId.equals(record.getUserId()) && id.equals(record.getId()))
                .findFirst()
                .orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public boolean existsById(final String userId, final String id) throws AbstractException {
        findOneById(userId, id);
        return true;
    }

    @Override
    public M remove(final String userId, final M record) {
        if (userId.equals(record.getUserId())) {
            records.remove(record);
        }
        return record;
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) throws ModelNotFoundException {
        final Optional<M> record = Optional.ofNullable(findOneByIndex(userId, index));
        record.ifPresent(records::remove);
        record.orElseThrow(ModelNotFoundException::new);
        return record.get();
    }

    @Override
    public M removeById(final String userId, final String id) throws AbstractException {
        final Optional<M> record = Optional.ofNullable(findOneById(userId, id));
        record.ifPresent(records::remove);
        record.orElseThrow(ModelNotFoundException::new);
        return record.get();
    }

    @Override
    public long totalCount(final String userId) {
        return records.stream()
                .filter(record -> userId.equals(record.getUserId()))
                .count();
    }

    @Override
    public void clear(final String userId) {
        removeAll(findAll(userId));
    }
}
