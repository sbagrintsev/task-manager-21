package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.bagrintsev.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;

public interface ICommandService extends Checkable {

    Collection<AbstractCommand> getAvailableCommands();

    void add(AbstractCommand command);

    AbstractCommand getCommandByName(String name) throws AbstractException;

    AbstractCommand getCommandByShort(String shortName) throws AbstractException;
}
