package ru.tsc.bagrintsev.tm.api.sevice;

public interface ILoggerService extends Checkable {

    void info(String message);

    void command(String message);

    void debug(String message);

    void error(Exception e);

}
