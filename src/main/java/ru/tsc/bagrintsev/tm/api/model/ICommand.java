package ru.tsc.bagrintsev.tm.api.model;

public interface ICommand {

    String getName();

    String getShortName();

    String getDescription();

}
