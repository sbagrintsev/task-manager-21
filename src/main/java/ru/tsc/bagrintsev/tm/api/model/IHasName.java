package ru.tsc.bagrintsev.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
