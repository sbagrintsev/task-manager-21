package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.field.AbstractFieldException;
import ru.tsc.bagrintsev.tm.exception.user.AbstractUserException;
import ru.tsc.bagrintsev.tm.model.User;

import java.security.GeneralSecurityException;

public interface IUserService extends IAbstractService<User>{

    User findByLogin(String login) throws AbstractException;

    User findByEmail(String email) throws AbstractException;

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    User removeByLogin(String login) throws AbstractException;

    User create(String login, String password) throws GeneralSecurityException, AbstractException;

    User setParameter(User user,
                      EntityField paramName,
                      String paramValue) throws AbstractException;

    User setRole(User user, Role role) throws AbstractException;

    User setPassword(String userId, String password) throws AbstractException, GeneralSecurityException;

    User updateUser(
            String userId,
            String firstName,
            String lastName,
            String middleName
    ) throws AbstractException;
}
