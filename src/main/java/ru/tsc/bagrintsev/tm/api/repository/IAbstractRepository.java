package ru.tsc.bagrintsev.tm.api.repository;

import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.AbstractEntityException;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectIndexException;
import ru.tsc.bagrintsev.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IAbstractRepository<M extends AbstractModel> {

    M add(M record) throws AbstractEntityException;

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M findOneByIndex(Integer index) throws IncorrectIndexException;

    M findOneById(String id) throws AbstractException;

    boolean existsById(String id) throws AbstractException;

    M remove(M model) throws AbstractException;

    M removeByIndex(Integer index) throws AbstractException;

    M removeById(String id) throws AbstractException;

    int totalCount();

    void clear();

    void removeAll(final Collection<M> collection);
}
