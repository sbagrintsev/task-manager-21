package ru.tsc.bagrintsev.tm.component;

import ru.tsc.bagrintsev.tm.api.repository.ICommandRepository;
import ru.tsc.bagrintsev.tm.api.repository.IProjectRepository;
import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.api.repository.IUserRepository;
import ru.tsc.bagrintsev.tm.api.sevice.*;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.command.project.*;
import ru.tsc.bagrintsev.tm.command.system.*;
import ru.tsc.bagrintsev.tm.command.task.*;
import ru.tsc.bagrintsev.tm.command.user.*;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.field.AbstractFieldException;
import ru.tsc.bagrintsev.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.bagrintsev.tm.exception.system.CommandNotSupportedException;
import ru.tsc.bagrintsev.tm.exception.user.AbstractUserException;
import ru.tsc.bagrintsev.tm.repository.CommandRepository;
import ru.tsc.bagrintsev.tm.repository.ProjectRepository;
import ru.tsc.bagrintsev.tm.repository.TaskRepository;
import ru.tsc.bagrintsev.tm.repository.UserRepository;
import ru.tsc.bagrintsev.tm.service.*;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;
import java.security.GeneralSecurityException;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new AboutCommand());
        registry(new ArgumentsCommand());
        registry(new CommandsCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new InfoCommand());
        registry(new VersionCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserRemoveCommand());
        registry(new UserSignInCommand());
        registry(new UserSignOutCommand());
        registry(new UserSignUpCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
    }

    public void run(final String[] args) throws IOException {
        initLogger();
        try {
            processOnStart(args);
        } catch (AbstractException | GeneralSecurityException e) {
            loggerService.error(e);
        }
        try {
            initUsers();
        } catch (GeneralSecurityException | AbstractException e) {
            System.err.println("User initialization error...");
            loggerService.error(e);
        }
        try {
            initData();
        } catch (AbstractException e) {
            System.err.println("Data initialization error...");
            loggerService.error(e);
        }
        while (true) {
            try {
                System.out.println();
                System.out.println("Enter Command:");
                System.out.print(">> ");
                final String command = TerminalUtil.nextLine();
                processOnTheGo(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void registry(AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initData() throws AbstractException {
        taskService.create("first task", "task simple description");
        taskService.create("second task", "task simple description");
        taskService.create("third task", "task simple description");
        taskService.create("fourth task", "task simple description");

        projectService.create("first project", "project simple description");
        projectService.create("second project", "project simple description");
        projectService.create("third project", "project simple description");
        projectService.create("fourth project", "project simple description");
    }

    private void initLogger() {
        loggerService.info("*** Welcome to Task Manager ***");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("*** Task Manager is shutting down ***");
            }
        });
    }

    private void initUsers() throws GeneralSecurityException, AbstractException {
        userService.create("test", "test").setEmail("test@test.ru");
        userService.create("admin", "admin").setRole(Role.ADMIN);
    }

    private void processOnStart(final String arg) throws AbstractException, IOException, GeneralSecurityException {
        final AbstractCommand abstractCommand = commandService.getCommandByShort(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processOnTheGo(final String command) throws IOException, AbstractException, GeneralSecurityException {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processOnStart(final String[] args) throws AbstractException, IOException, GeneralSecurityException {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        processOnStart(arg);
        commandService.getCommandByName("exit").execute();
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }
}
