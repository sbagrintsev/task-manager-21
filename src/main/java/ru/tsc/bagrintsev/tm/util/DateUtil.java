package ru.tsc.bagrintsev.tm.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public interface DateUtil {

    SimpleDateFormat format = new SimpleDateFormat("d.MM.y H:m:ss");

    static Date toDate(final String value) {
        try {
            return format.parse(value);
        } catch (ParseException e) {
            System.out.println("WRONG DATE FORMAT!!!");
        }
        return null;
    }

    static String toString(final Date date) {
        if (date == null) return "";
        return format.format(date);
    }

}
