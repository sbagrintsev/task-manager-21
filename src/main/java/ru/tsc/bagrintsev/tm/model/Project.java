package ru.tsc.bagrintsev.tm.model;

import ru.tsc.bagrintsev.tm.api.model.IWBS;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.util.DateUtil;

import java.util.Date;

public final class Project extends AbstractUserOwnedModel implements IWBS {

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private Date dateCreated = new Date();

    private Date dateStarted;

    private Date dateFinished;

    public Date getDateCreated() {
        return dateCreated;
    }

    @Override
    public void setDateCreated(final Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateStarted() {
        return dateStarted;
    }

    public void setDateStarted(final Date dateStarted) {
        this.dateStarted = dateStarted;
    }

    public Date getDateFinished() {
        return dateFinished;
    }

    public void setDateFinished(final Date dateFinished) {
        this.dateFinished = dateFinished;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }


    public void setStatus(final Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return name + " : " + description +
                "\n\tid: " + getId() +
                "\n\tstatus: " + status +
                "\tcreated: " + DateUtil.toString(dateCreated) +
                "\tstarted: " + DateUtil.toString(dateStarted) +
                "\tfinished: " + DateUtil.toString(dateFinished);
    }

}
